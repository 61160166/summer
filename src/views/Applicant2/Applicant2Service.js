const applicant2Service = {
  userList: [
    // {
    //   id: 1,
    //   firstName: 'Rujirada',
    //   lastName: 'Norasarn',
    //   gender: 'F',
    //   birthday: '1999-01-27',
    //   nationality: 'Thai',
    //   tel: '061-666-6666',
    //   address: 'Thai'
    // },
    // {
    //   id: 2,
    //   firstName: 'Kantapon',
    //   lastName: 'Aonraksa',
    //   gender: 'F',
    //   birthday: '2000-07-17',
    //   nationality: 'Thai',
    //   tel: '069-555-4566',
    //   address: 'Thai'
    // }
  ],
  lastId: 3,
  addUser (user) {
    user.id = this.lastId++
    this.userList.push(user)
  },
  updateUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1, user)
  },
  deleteUser (user) {
    const index = this.userList.findIndex(item => item.id === user.id)
    this.userList.splice(index, 1)
  },
  getapplicant2 () {
    return [...this.userList]
  }
}

export default applicant2Service
